/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer;

import javazoom.jlgui.basicplayer.BasicController;
import javazoom.jlgui.basicplayer.BasicPlayerEvent;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.PlayerStates;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.gui.PlayerGUI;
import net.adamslo.fontanaplayer.util.Dictionary;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Map;

/**
 * FontanaPlayer Uses GUI Graphics interface to bring own functionality to user
 * friendly interface.
 * Class implements the functionalyty of state changing, the keyboard input
 * handling, creating lyrics a song lists. It also runs or stops the MP3
 * FontanaPlayer
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 1.0,  &nbsp; 28-SEP-2009
 * @since SDK1.5
 */
class FontanaPlayer extends PlayerGUI {


    /**
     * instance lyrics includes all first pages from all the pdf's in specified
     * path - the constructor's parameter
     * <p>
     * Make shure path exists
     */
    LyricsList lyrics;
    /**
     * Variable state keeps the actual players's  state. Default is START state
     */
    PlayerStates state = PlayerStates.START;
    /**
     * the current page of pdf document
     */
    int currentPage = 0;
    /**
     * The number of pages in current pdf page
     */
    int numberOfPages = 0;

    /**
     * player is the intance class uses to handle playing songs. The constructor
     * loads all the song in specified path - the constructor's parameter.
     * <p>
     * Make shure path does exists
     */
    private MP3Player player;
    /**
     * varible identifier server to store the character sequence specifies
     * selected song and lyrics
     */
    private String identifier = "";
    /**
     * if true music is played when song exists. Else even the song exists
     * player ignores it
     */
    private boolean musicEnabled = true;
    /**
     * Listener use if light weight mode is set to be false
     */
    private BasicPlayerListener basicPlayerListener;

    FontanaPlayer() {

    }

    /**
     * Use to know if the charactes is a letter or number
     *
     * @param c character usualy from ASCII table
     * @return true if parameter is letter or number
     */
    @Contract(pure = true)
    private boolean isAplhanumeric(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9');
    }

    /**
     * this method shoud handle the input key - handling changing states, start and
     * playing dong, drawing lyrics etc.
     *
     * @param evt KeyEvent to digg out key code
     */
    @Override
    public void handleKeyInput(@NotNull KeyEvent evt) {
        switch (evt.getKeyCode()) {
            /*
             *Application is terminated and closed by pressing escape
             */
            case KeyEvent.VK_ESCAPE:
                terminate();
                break;
            /*
             just restasrt curently played song
             */
            case KeyEvent.VK_F12:
                this.player.restart();
                break;
            /*
             *Scrolling the main text field
             */

            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
                textLabel.setBackground(Color.GREEN);
                int inverse = 1;
                if (evt.getKeyCode() == KeyEvent.VK_UP) {
                    inverse = -1;
                }

                Rectangle visibleText = pagePanel.getVisibleRect();
                int y1 = (int) visibleText.getY();
                visibleText.setLocation((int) visibleText.getX(), (int) visibleText.getY() + PlayerSetup.getInteger(SetupIdentifiers.SCROLLINCREMENT) * inverse);

                pagePanel.scrollRectToVisible(visibleText);
                int y2 = (int) pagePanel.getVisibleRect().getY();
                //scrolled to lowest position
                if ((int) pagePanel.getVisibleRect().getY() >= pagePanel.getHeight() - LyricsScrollArea.getHeight()
                        || (int) pagePanel.getVisibleRect().getY() == 0) {
                    textLabel.setBackground(Color.orange);
                }
                if (y1 == y2) {
                    if (inverse == -1) {
                        if (currentPage == 0) {
                            break;
                        }
                        pagePanel.showPage(lyrics.getPrevious());
                        visibleText.setLocation((int) visibleText.getX(), 10000);
                        pagePanel.scrollRectToVisible(visibleText);
                        numberOfPages = lyrics.getNumberOfPages();
                        currentPage = lyrics.getcurrentPage();
                        textLabel.setText(getTextLabel());
                    } else {
                        if (currentPage == numberOfPages - 1) {
                            break;
                        }
                        pagePanel.showPage(lyrics.getNext());
                        visibleText.setLocation((int) visibleText.getX(), 0);
                        pagePanel.scrollRectToVisible(visibleText);
                        numberOfPages = lyrics.getNumberOfPages();
                        currentPage = lyrics.getcurrentPage();
                        textLabel.setText(getTextLabel());
                    }
                }
                break;

            /*
             *Pausing or unpausing song - if is played
             */

            case KeyEvent.VK_SPACE:
                if (player.isPaused()) {
                    player.resume();
                    musicLabel.setBackground(Color.green);
                } else {
                    musicLabel.setBackground(blue);
                    player.pause();
                }
                break;
            /*
            Status is changed only by enter key. It depends on each the state
            wicth action will be taken
             */
            case KeyEvent.VK_ENTER:
                switch (state) {
                    /*There is nowhere to move from start position whet not even
                     * one letter was typped.
                     * May be used to let user know what to do when runs
                     * application
                     */
                    case START:
                        break;
                    /*
                     *In case user has already typed 3 valid characters the
                     * lyrics are loaded. Also the scroll pane with lyrics
                     * is scrolled up
                     *
                     * Some GUI elements are repainter to let user know
                     * he entered out of this state
                     *
                     * The state is than changed to PRINT_TEXT
                     */
                    case TYPING:
                        if (identifier.length() == 3) {
                            double heightMultipler = lyrics.getHeightMultipler();
                            if (heightMultipler == 0) {
                                heightMultipler = PlayerSetup.getInteger(SetupIdentifiers.HEIGTHMULTIPLER);
                            }
                            changeLyricsPanelDimensions(heightMultipler);
                            Rectangle rctg = pagePanel.getVisibleRect();
                            rctg.setLocation(0, 0);
                            pagePanel.repaint();
                            pagePanel.scrollRectToVisible(rctg);

                            pagePanel.showPage(lyrics.get(identifier));
                            numberOfPages = lyrics.getNumberOfPages();
                            currentPage = lyrics.getcurrentPage();
                            textLabel.setText(getTextLabel());

                            state = PlayerStates.PRINT_TEXT;
                            textLabel.setBackground(Color.green);
                            selectBorder(textLabel);
                            sendMessageToClient(identifier);

                        }
                        break;
                    /*
                     *The text is already printed out. Now is the song loaded
                     * and played. If no song found, player is stoped
                     *
                     * state is than changed to PLAY_MUSIC
                     */
                    case PRINT_TEXT:
                        state = PlayerStates.PLAY_MUSIC;
                        musicLabel.setBackground(Color.green);
                        selectBorder(this.musicLabel);

                        if (musicEnabled) {
                            if (!player.play(identifier)) {
                                clearPlayerPanel();
                            }
                        } else {
                            player.stop();

                            clearPlayerPanel();
                        }

                        break;
                    /*
                     *Music is beeing played. when user oress enter it stops
                     *playing it. Not pause. there is no way how to continue
                     * but enter the new characters and double enter
                     *
                     * state is returned into start position
                     */
                    case PLAY_MUSIC:
                        musicLabel.setBackground(red);
                        textLabel.setBackground(red);
                        lettersLabel.setBackground(red);
                        selectBorder(lettersLabel);
                        identifier = "";
                        lettersLabel.setText("???");
                        player.stop();
                        clearPlayerPanel();
                        state = PlayerStates.START;
                        break;
                }

                break;
            /*
             * if no special key is pressed only alphanumeric keys are
             * proccesed. The identifier of song has to be alfanumeric so there
             * is no need to check other keys.
             *
             * When identifier is already of lenght 3, identifier is deleted and
             * the actual character is the first letter in identifier.
             */
            default:
                if (isAplhanumeric(evt.getKeyChar())) {
                    state = PlayerStates.TYPING;
                    selectBorder(lettersLabel);
                    if (identifier.length() < 3) {
                        identifier += evt.getKeyChar();
                    } else {

                        identifier = String.valueOf(evt.getKeyChar());
                    }
                    lettersLabel.setBackground(Color.green);
                    lettersLabel.setText(identifier.toUpperCase());
                }
                break;
        }
    }

    /**
     * Actions taken when application terminating. FontanaPlayer is stopped before it.
     */
    @Override
    protected void terminate() {
        player.stop();
        dispose();

    }

    /**
     * Extends a group of action taken while initialization. If lightWeight
     * Option is false, playback listener is addes to paint out progress bar
     * of song that is played
     */
    @Override
    public void plusInit() {
        lyrics = new LyricsList();
        player = new MP3Player();
        pagePanel.showPage(lyrics.get(PlayerSetup.getString(SetupIdentifiers.INTROIDENTIFIER)));

        if (!PlayerSetup.getBoolean(SetupIdentifiers.LIGHTWEIGHTMODE)) {
            addPlayerListener();
        } else {
            removePlayerListener();

        }
        textLabel.setText(getTextLabel());

        this.translate();

    }

    @NotNull
    String getTextLabel() {
        return Dictionary.get(DictionaryIdentifiers.LYRICS) + " (" + (currentPage + 1) + "/" + numberOfPages + ")";
    }

    /**
     * ads listener for player and specify its behavior.
     */
    private void addPlayerListener() {
        if (PlayerSetup.getBoolean(SetupIdentifiers.LIGHTWEIGHTMODE)) {
            return;
        }
        basicPlayerListener = new BasicPlayerListener() {

            private int seconds;
            private int second;

            public void opened(Object stream, Map properities) {
                playerPanel.setPlayerPanelProgressbarMin();
                playerPanel.setPlayerPanelProgressbarValue(0);
                playerPanel.setPlayerPanelProgressbarMax(Integer.parseInt(properities.get("mp3.length.frames").toString()));
                playerPanel.setPlayerPanelSongNameText(identifier.toUpperCase());
            }

            public void progress(int bytesread, long microseconds, byte[] pcmdata, Map properities) {
                playerPanel.setPlayerPanelProgressbarValue(Integer.parseInt(properities.get("mp3.frame").toString()));
                playerPanel.repaint();
                seconds = (int) (Double.valueOf(properities.get("mp3.position.microseconds").toString()) / 1000000);
                second = (int) ((double) seconds) % 60;
                playerPanel.setPlayerPanelSecondsLeftText((seconds / 60) + ":" + ((second < 10) ? "0" + second : second));
            }

            public void stateUpdated(BasicPlayerEvent event) {
            }

            public void setController(BasicController controler) {
            }
        };
        player.addBasicPlayerListener(basicPlayerListener);

        playerPanel.setVisible(true);
    }

    /**
     * removes the listener. Song progressbar is set not to be visible and
     * swith on controler appears
     */
    private void removePlayerListener() {
        if (basicPlayerListener != null) {
            player.removeBasicPlayerListener(basicPlayerListener);
        }
        playerPanel.setVisible(false);
    }

    /**
     * When the music is about to be disabled, the label is changed and private
     * variable musicEnabled is set. This variable is used by player to recognize
     * if song should be played or not.
     */
    @Override
    public void toogleMusic() {
        if (musicEnabled) {
            stopMusic();
        } else {
            playMusic();
        }
        musicEnabled = !musicEnabled;
    }

    private void stopMusic() {
        musicLabel.setText(Dictionary.get(DictionaryIdentifiers.MUSICOFF));
        player.stop();
        clearPlayerPanel();
        removePlayerListener();
    }

    private void playMusic() {
        musicLabel.setEnabled(true);
        musicLabel.setText(Dictionary.get(DictionaryIdentifiers.MUSICON));
        addPlayerListener();

    }
    /**
     * changes dimensions - height of page panel according to new height mulipler
     *
     * @param heightMultipler is new height multipler
     */
    void changeLyricsPanelDimensions(double heightMultipler) {
        LyricsPanel.setPreferredSize(new Dimension(Toolkit.getDefaultToolkit().getScreenSize().width, (int) (Toolkit.getDefaultToolkit().getScreenSize().width * heightMultipler)));
        LyricsPanel.revalidate();
        LyricsPanel.repaint();
    }

    /**
     * clears player panel if no song is played
     */
    private void clearPlayerPanel() {
        playerPanel.setPlayerPanelSecondsLeftText(" ");
        playerPanel.setPlayerPanelSongNameText(" ");
        playerPanel.setPlayerPanelProgressbarValue(0);
    }

    private void translate() {
        closeButton.setText(Dictionary.get(DictionaryIdentifiers.CLOSEBUTTON));
        musicLabel.setText(Dictionary.get(DictionaryIdentifiers.MUSICON));
        textLabel.setText(Dictionary.get(DictionaryIdentifiers.LYRICS));
    }

    protected void toogleConnectionServer() {
    }

    protected void toogleConnectionClient() {
    }

    void sendMessageToClient(String identifier) {
    }
}
