/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.debug;

import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.gui.PlayerGUI;
import net.adamslo.fontanaplayer.util.Dictionary;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;


public class ScreenPrinter {

    public static void p(@Nullable DictionaryIdentifiers o, @Nls Object... replaces) {
        printStringRaw(Dictionary.get(o, replaces));
    }

    public static void p(@Nullable String s) {
        printStringRaw(s);
    }

    private static void printStringRaw(@Nullable String printText) {
        JLabel label = PlayerGUI.getLogLabel();
        if (label != null) {
            label.setText(printText);
        }
    }
}
