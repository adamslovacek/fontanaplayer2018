/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.debug;


import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FilePrinter {

    private static final String extension = "log.txt";

    public static void p(@Nullable Object p) {
        String s = String.valueOf(p);
        s = s == null ? "null" : s;

        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("." + extension, true));
            bw.write(s);
            bw.newLine();
            bw.flush();
        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            if (bw != null) try {
                bw.close();
            } catch (IOException ex) {
                ScreenPrinter.p(DictionaryIdentifiers.ERRORFILEPRINTER, ex.getMessage());
            }
        }
    }

    private static void printToBuffer(BufferedWriter bw, String s) throws IOException {
        bw = new BufferedWriter(new FileWriter("." + extension, true));
        bw.write(s == null ? "null" : s);
        bw.newLine();
        bw.flush();
    }
}
