/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.gui;

import org.jetbrains.annotations.NotNull;

import java.awt.*;

public class PlayerPanel extends javax.swing.JPanel {

    private final Color backgroundColor;
    private javax.swing.JProgressBar playerPanelProgressbar;
    private javax.swing.JLabel playerPanelSecondsLeft;
    private javax.swing.JLabel playerPanelSongName;


    PlayerPanel(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
        initComponents();
    }

    private void initComponents() {
        initPanelSongName();
        initPanelSecondLeft();
        initPanelProgressbar();

        setBackground(backgroundColor);
        setLayout(null);
    }

    private void initPanelProgressbar() {
        playerPanelProgressbar = new javax.swing.JProgressBar();
        playerPanelProgressbar.setBackground(backgroundColor);
        playerPanelProgressbar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        playerPanelProgressbar.setBounds(0, 20, 200, 27);
        add(playerPanelProgressbar);
    }

    private void initPanelSecondLeft() {
        playerPanelSecondsLeft = new javax.swing.JLabel();
        playerPanelSecondsLeft.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        playerPanelSecondsLeft.setText(" ");
        playerPanelSecondsLeft.setBounds(100, 0, 100, 20);
        add(playerPanelSecondsLeft);
    }

    private void initPanelSongName() {
        playerPanelSongName = new javax.swing.JLabel();
        playerPanelSongName.setFont(new java.awt.Font("Tahoma", Font.BOLD, 11));
        playerPanelSongName.setText(" ");
        playerPanelSongName.setBounds(0, 0, 100, 20);
        add(playerPanelSongName);
    }

    public void setPlayerPanelSecondsLeftText(@NotNull String text) {
        playerPanelSecondsLeft.setText(text);
    }

    public void setPlayerPanelSongNameText(@NotNull String text) {
        playerPanelSongName.setText(text);
    }

    public void setPlayerPanelProgressbarMin() {
        playerPanelProgressbar.setMinimum(0);

    }

    public void setPlayerPanelProgressbarMax(int max) {
        playerPanelProgressbar.setMaximum(max);
    }

    public void setPlayerPanelProgressbarValue(int value) {
        playerPanelProgressbar.setValue(value);
    }

}
