/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.gui;

import com.sun.pdfview.PagePanel;
import net.adamslo.fontanaplayer.PlayerSetup;
import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.util.Dictionary;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;


public abstract class PlayerGUI extends JFrame {

    private static JLabel logLabel = null;
    /**
     * pagePanel is the PagePanel where are the Lyrics painted
     */
    protected final PagePanel pagePanel = new PagePanel();
    protected final Color blue = new Color(153, 153, 255);
    protected final Color red = new Color(149, 90, 90);
    /**
     * Border for not selected state Sign
     */
    private final Border unselectedBorder = BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 3);
    private final int bottomButtonHeigth = 40;
    private final Dimension square = new Dimension(40, bottomButtonHeigth);
    private final Color backgrouColor = Color.WHITE;
    private final Border selectedBorder = BorderFactory.createLineBorder(new Color(0, 0, 0), 3);
    protected JPanel LyricsPanel;
    protected JScrollPane LyricsScrollArea;
    protected PlayerPanel playerPanel;
    protected JLabel lettersLabel;
    protected JLabel musicLabel;
    protected JLabel textLabel;
    protected JButton closeButton;
    protected JButton connectButtonServer;
    protected JButton connectButtonClient;
    /**
     * PlayerUI UI initialization. The window is set to be in fullcreen mode.
     * Baceuse UI handles all the keyboard inputs it grabs the focus.
     */
    protected PlayerGUI() {
        initComponents();
        forceFocus();
        plusInit();

    }

    @Nullable
    public static JLabel getLogLabel() {
        return logLabel;
    }

    /**
     * Highlights the selected JLabel instaces border and dehighlights the rest
     * of them
     *
     * @param label the JLabel instace that's border should be highlighted
     */
    protected void selectBorder(@NotNull JLabel label) {
        musicLabel.setBorder(unselectedBorder);
        textLabel.setBorder(unselectedBorder);
        lettersLabel.setBorder(unselectedBorder);
        label.setBorder(selectedBorder);
    }

    private void initComponents() {

        setFrameProperities();

        getContentPane().setLayout(new BorderLayout());
        getContentPane().setBackground(backgrouColor);

        addKeyListener(new java.awt.event.KeyAdapter() {

            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        add(getScrolableArea(), BorderLayout.NORTH);
        add(getControlPanel(), BorderLayout.SOUTH);

        validate();
        repaint();
        pack();

        if (fullscreenMode()) {
            setExtendedState(Frame.MAXIMIZED_BOTH);
        }
    }

    @NotNull
    private Boolean fullscreenMode() {
        return PlayerSetup.getCustom("windowmode") == null || !Boolean.valueOf(PlayerSetup.getCustom("windowmode").toString());
    }

    private void setFrameProperities() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle(Dictionary.get(DictionaryIdentifiers.WINDOWTITLE));
        setAlwaysOnTop(true);
        setBackground(backgrouColor);
        setMinimumSize(null);
        if (fullscreenMode()) {
            setUndecorated(true);
        }
    }

    private JScrollPane getScrolableArea() {
        int divider = fullscreenMode() ? 1 : 2;

        int ScreenW = Toolkit.getDefaultToolkit().getScreenSize().width / divider;
        int ScreenH = Toolkit.getDefaultToolkit().getScreenSize().height / divider;

        LyricsPanel = this.pagePanel;
        LyricsScrollArea = new JScrollPane(LyricsPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        LyricsPanel.setBackground(backgrouColor);
        LyricsScrollArea.setBackground(backgrouColor);

        LyricsPanel.setPreferredSize(new Dimension(ScreenW, ScreenH - 130));
        LyricsScrollArea.setBounds(0, 0, ScreenW, ScreenH - 120);
        LyricsScrollArea.setPreferredSize(new Dimension(ScreenW, ScreenH - 120));
        LyricsPanel.setVisible(true);
        LyricsScrollArea.setOpaque(false);

        return LyricsScrollArea;
    }

    private JPanel getControlPanel() {
        JPanel controlPanel = new JPanel();
        setControPanelProperities(controlPanel);

        controlPanel.add(getEastPanel(), BorderLayout.EAST);
        controlPanel.add(getWestPanel(), BorderLayout.WEST);
        controlPanel.add(getLogoLabel(), BorderLayout.SOUTH);

        playerPanel = new PlayerPanel(backgrouColor);
        controlPanel.add(playerPanel);

        return controlPanel;
    }

    private void setControPanelProperities(JPanel controlPanel) {
        controlPanel.setBackground(backgrouColor);
        controlPanel.setMaximumSize(new Dimension(32767, 60));
        controlPanel.setMinimumSize(new Dimension(640, 60));
        controlPanel.setPreferredSize(new Dimension(855, 60));
        controlPanel.setRequestFocusEnabled(false);
        controlPanel.setLayout(new BorderLayout());
    }

    private JLabel getLogoLabel() {
        JLabel logoLabel = new JLabel();
        PlayerGUI.logLabel = logoLabel;
        logoLabel.setFont(new Font("Tahoma", Font.ITALIC, 10));
        logoLabel.setHorizontalAlignment(SwingConstants.CENTER);
        logoLabel.setText(PlayerSetup.getString(SetupIdentifiers.LOGO));
        return logoLabel;
    }

    private JPanel getWestPanel() {
        JPanel west = new JPanel();

        initCloseButton();
        west.add(closeButton);

        initConnectButtonClient();
        west.add(connectButtonClient);

        initConnectButtonServer();
        west.add(connectButtonServer);
        return west;
    }

    private JPanel getEastPanel() {
        JPanel east = new JPanel();

        initLettersLabel();
        east.add(lettersLabel);

        initTextLabel();
        east.add(textLabel);

        initMusicLabel();
        east.add(musicLabel);

        return east;
    }

    private void initTextLabel() {
        textLabel = new JLabel();

        textLabel.setMaximumSize(new Dimension(50, bottomButtonHeigth));
        textLabel.setMinimumSize(new Dimension(50, bottomButtonHeigth));
        textLabel.setPreferredSize(new Dimension(100, bottomButtonHeigth));

        textLabel.setBackground(new Color(149, 90, 90));
        textLabel.setHorizontalAlignment(SwingConstants.CENTER);
        textLabel.setText("TEXT");
        textLabel.setBorder(BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 2));
        textLabel.setOpaque(true);
        textLabel.setRequestFocusEnabled(false);


    }

    private void initLettersLabel() {
        lettersLabel = new JLabel();

        lettersLabel.setMaximumSize(new Dimension(50, bottomButtonHeigth));
        lettersLabel.setMinimumSize(new Dimension(50, bottomButtonHeigth));
        lettersLabel.setPreferredSize(new Dimension(100, bottomButtonHeigth));

        lettersLabel.setBackground(new Color(149, 90, 90));
        lettersLabel.setFont(new Font("Tahoma", Font.PLAIN, 24)); // NOI18N
        lettersLabel.setHorizontalAlignment(SwingConstants.CENTER);
        lettersLabel.setText("???");
        lettersLabel.setBorder(BorderFactory.createLineBorder(new Color(0, 0, 0), 2));
        lettersLabel.setOpaque(true);
        lettersLabel.setRequestFocusEnabled(false);

    }

    private void initConnectButtonClient() {
        connectButtonClient = new JButton(Dictionary.get(DictionaryIdentifiers.CLIENTBUTTON));
        setSquareDimensions(connectButtonClient);
        connectButtonClient.setBackground(blue);
        connectButtonClient.setBorder(BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 3));
        connectButtonClient.setRequestFocusEnabled(false);

        connectButtonClient.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseClicked(java.awt.event.MouseEvent evt) {
                toogleConnectionClient();
            }
        });
    }

    private void initCloseButton() {
        closeButton = new JButton();
        setSquareDimensions(closeButton);

        closeButton.setBounds(3, 3, 70, bottomButtonHeigth);
        closeButton.setBackground(blue);
        closeButton.setText(Dictionary.get(DictionaryIdentifiers.CLOSEBUTTON));
        closeButton.setBorder(BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 3));

        closeButton.setRequestFocusEnabled(false);

        closeButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                closeButtonMouseClicked();
            }
        });

    }

    private void initConnectButtonServer() {

        connectButtonServer = new JButton(Dictionary.get(DictionaryIdentifiers.SERVERBUTTON));
        connectButtonServer.setBackground(blue);
        connectButtonServer.setBorder(BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 3));
        setSquareDimensions(connectButtonServer);
        connectButtonServer.setRequestFocusEnabled(false);
        connectButtonServer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                toogleConnectionServer();
            }
        });

    }

    private void setSquareDimensions(JButton button) {

        button.setMaximumSize(square);
        button.setMinimumSize(square);
        button.setPreferredSize(square);
    }

    private void initMusicLabel() {
        musicLabel = new JLabel();

        musicLabel.setMaximumSize(new Dimension(50, bottomButtonHeigth));
        musicLabel.setMinimumSize(new Dimension(50, bottomButtonHeigth));
        musicLabel.setPreferredSize(new Dimension(100, bottomButtonHeigth));

        musicLabel.setBackground(new Color(149, 90, 90));
        musicLabel.setHorizontalAlignment(SwingConstants.CENTER);
        musicLabel.setBorder(BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 2));
        musicLabel.setOpaque(true);
        musicLabel.setRequestFocusEnabled(false);

        musicLabel.addMouseListener(new java.awt.event.MouseAdapter() {

            public void mouseClicked(java.awt.event.MouseEvent evt) {
                musicLabelMouseClicked();
            }
        });
    }

    protected abstract void toogleConnectionServer();

    protected abstract void toogleConnectionClient();

    private void formKeyPressed(@NotNull java.awt.event.KeyEvent evt) {
        handleKeyInput(evt);
    }

    private void musicLabelMouseClicked() {
        toogleMusic();
    }

    private void closeButtonMouseClicked() {
        terminate();
    }

    /**
     * Interface needs to have key focus to be able to handle keyboard inputs
     */
    private void forceFocus() {

        setVisible(true);
        toFront();
        repaint();
        requestFocus();
    }


    /**
     * switches music on or of
     */
    protected abstract void toogleMusic();

    /**
     * What to do in extended class when initialization of GUI is finished
     */
    protected abstract void plusInit();

    /**
     * this method shoud handle the input key - handling changing states, start and
     * playing dong, drawing lyrics etc.
     *
     * @param evt KeyEvent to digg out key code
     */
    protected abstract void handleKeyInput(java.awt.event.KeyEvent evt);

    /**
     * Method called when the application is terminates
     */
    protected abstract void terminate();


}
