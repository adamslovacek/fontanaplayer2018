/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;

/**
 * This enum represents the FontanaPlayer states
 * <!--
 * _______             ________         ____________          ___________
 * |       | Alphanum. |        | Enter |            | Enter  |            |
 * | START |--------- >| TYPING |----- >| PRINT_TEXT |------ >| PLAY_MUSIC |
 * |_______|           |________|       |____________|        |____________|
 * ^                 ^  ^              |                   |    |
 * |                 |  |___Alphanum.__|                   |    |
 * |                 |______________Alphanum.______________|    |
 * |__________________________Enter_____________________________|
 * <p>
 * <p>
 * //-->
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 1.0,  &nbsp; 28-SEP-2009
 * @since SDK1.5
 */
public enum PlayerStates {

    /**
     * default state after application starts
     */
    START,
    /**
     * user typed some string - identificator. Wait for enter to print pdf
     */
    TYPING,
    /**
     * text is printed. wait for enter to move into PLAY_MUSIC state
     * or user may move to TYPING by pressing alphanumeric key
     */
    PRINT_TEXT,
    /**
     * music is played. It is stopped by enter and application moves into
     * START state. User may also type alphanumeric key to move into TYPING state
     * while music is not stopped
     */
    PLAY_MUSIC
}
