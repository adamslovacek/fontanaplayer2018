/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum SetupIdentifiers {

    LOGO("LOGO", String.class),
    GREY("GREY", String[].class),
    POEDITORTOKENFILE("POETOKEN", String.class),
    POEDITORPROJECT("POEPROJECT", String.class),
    POEDITORCACHEFILE("POEFILE", String.class),
    POEDITORLANGUAGES("POELANGUAGES", String[].class),
    LANGUAGE("LANGUAGE", String.class),
    SCROLLINCREMENT("SCROLLINCREMENT", Integer.class),
    HEIGTHMULTIPLER("HEIGTHMULTIPLER", Integer.class),
    INTROIDENTIFIER("INTROIDENTIFIER", String.class),
    SONGFOLDER("SONGFOLDER", String.class),
    LYRICSFOLDER("LYRICSFOLDER", String.class),
    MAXFILES("MAXFILES", Integer.class),
    LIGHTWEIGHTMODE("LIGHTWEIGHTMODE", Boolean.class),
    SINGLEPAGEINTERFACE("SINGLEPAGEINTERFACE", Boolean.class);

    private final String identifier;
    private final Class classname;

    SetupIdentifiers(String idn, Class stringClass) {
        identifier = idn;
        classname = stringClass;
    }

    @Nullable
    public static SetupIdentifiers getByName(@NotNull String name) {
        for (SetupIdentifiers d : SetupIdentifiers.values()) {
            if (d.getIdentifier().equals(name)) {
                return d;
            }
        }
        return null;
    }

    private String getIdentifier() {
        return identifier;
    }

    public String getClassname() {
        return classname.getName();
    }

}
