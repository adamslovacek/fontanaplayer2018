/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;

import org.jetbrains.annotations.Nullable;

import java.util.EventListener;


public interface NetworkListener extends EventListener {
    void loadLyrics(@Nullable String identifier);
}
