/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;


import org.jetbrains.annotations.NotNull;

public enum ServerStates {

    HELLO("hello"),
    BYE("bye"),
    LYRICS("lyrics");

    private final String s;

    ServerStates(@NotNull String s) {
        this.s = s;
    }

    @NotNull
    public String getString() {
        return s;
    }
}
