/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;

public enum NetrowkElements {
    SERVER,
    CLIENT
}
