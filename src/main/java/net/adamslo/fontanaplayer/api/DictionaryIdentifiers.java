/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.api;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum DictionaryIdentifiers {
    SERVERSTART("SERVERSTART"),
    SERVERSTOP("SERVERSTO"),
    CLIENTSTART("CLIENTSTART"),
    CLIENTSTOP("CLIENTSTOP"),
    ERRORMP3("ERRORMP3"),
    NOTFOUNDMP3("NOTFOUNDMP3"),
    FOUNDMP3("FOUNDMP3"),
    NOTFOUNDLYRICS("NOTFOUNDLYRICS"),
    FOUNDLYRICS("FOUNDLYRICS"),
    TRANSLATIONMISSING("TRANSLATIONMISSING"),
    BYESERVER("BYESERVER"),
    PAUSEABEL("PAUSEABEL"),
    PAUSEDESCRIPTION("PAUSEDESCRIPTION"),
    IDENTIFIERSLABEL("IDENTIFIERSLABEL"),
    IDENTIFIERSDESCRIPTION("IDENTIFIERSDESCRIPTION"),
    SCROLLLABEL("SCROLLLABEL"),
    SCROLLDESCRIPTION("SCROLLDESCRIPTION"),
    RESTARTLABEL("RESTARTLABEL"),
    OKLABEL("OKLABEL"),
    OKDESCRIPTION("OKDESCRIPTION"),
    CLOSELABEL("CLOSELABEL"),
    CLOSEDESCRIPTION("CLOSEDESCRIPTION"),
    CLOSEBUTTON("CLOSEBUTTON"),
    MUSICON("MUSICON"),
    MUSICOFF("MUSICOFF"),
    LYRICS("LYRICS"),
    BYECLIENT("BYECLIENT"),
    NOINPUTSERVER("NOINPUTSERVER"),
    UNABLEREADINPUTSTREAM("UNABLEREADINPUTSTREAM"),
    NOSERVER("NOSERVER"),
    ERRORFILEPRINTER("ERRORFILEPRINTER"),
    POEAPIERROR("POEAPIERROR"),
    ERRORFILETOKEN("ERRORFILETOKEN"),
    WINDOWTITLE("WINDOWTITLE"),
    SERVERBUTTON("SERVERBUTTON"),
    CLIENTBUTTON("CLIENTBUTTON");

    private final String displayName;

    DictionaryIdentifiers(String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static DictionaryIdentifiers getByName(@NotNull String name) {
        for (DictionaryIdentifiers d : DictionaryIdentifiers.values()) {
            if (d.getDisplayName().equals(name)) {
                return d;
            }
        }
        return null;
    }

    private String getDisplayName() {
        return displayName;
    }
}
