/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer;

import com.sun.pdfview.PDFPage;
import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import org.jetbrains.annotations.Nullable;

import java.io.File;

/**
 * Instance of this class is actualy the Map of SingleSongLyrics instances
 * with modified get() method. It also browses and decides wich files are to
 * be readed as a lyrics.
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 1.0,  &nbsp; 28-SEP-2009
 * @since SDK1.5
 */
class LyricsList {

    /**
     * The location of the pdfs
     */
    private final String path;
    /**
     * the path where the files are specified
     */
    private SingleSongLyrics songLyrics;

    /**
     * sets the directory with lyrics
     *
     */
    LyricsList() {
        this.path = PlayerSetup.getString(SetupIdentifiers.LYRICSFOLDER);

    }

    /**
     * opens pdf file and reads the first page. Then it returns it
     *
     * @param identifier the 3 letters representation of pdf file
     * @return the single page from choosed pdf file or null if no such a file
     */
    @Nullable
    PDFPage get(@Nullable String identifier) {
        if (identifier != null) {
            File f = new File(path + identifier + ".pdf");
            if (!f.exists()) {
                f = new File(path + identifier.toUpperCase() + ".pdf");
                if (!f.exists()) {
                    if (!identifier.equals("") && !identifier.equals(PlayerSetup.getString(SetupIdentifiers.INTROIDENTIFIER))) {
                        ScreenPrinter.p(DictionaryIdentifiers.NOTFOUNDLYRICS, identifier);
                    }
                    return null;
                }
            }
            songLyrics = new SingleSongLyrics(f);
            ScreenPrinter.p(DictionaryIdentifiers.FOUNDLYRICS, identifier);
            return songLyrics.getFirstPage();
        }
        return null;

    }

    /**
     * serves to obtain next page if exists
     *
     * @return next pdf page
     */
    @Nullable
    PDFPage getNext() {
        if (songLyrics == null) {
            return null;
        }
        return songLyrics.getNextPage();
    }

    /**
     * serves to obtain previous page if exists
     *
     * @return previous pdf page
     */
    @Nullable
    PDFPage getPrevious() {
        if (songLyrics == null) {
            return null;
        }
        return songLyrics.getPreviousPage();
    }
    /* Returns selected lyrics page height multipler
     * 
     * @param identifier a string identifier of lyrics
     * @return height multipler
     */

    double getHeightMultipler() {
        if (songLyrics == null) {
            return 0;
        }
        return songLyrics.getHeightMultipler();
    }

    int getNumberOfPages() {
        if (songLyrics == null) {
            return 0;
        }
        return songLyrics.getNumberOfPages();
    }

    int getcurrentPage() {
        if (songLyrics == null) {
            return 0;
        }
        return songLyrics.getCurrentPage();
    }
}
