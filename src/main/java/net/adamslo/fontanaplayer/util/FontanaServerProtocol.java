/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.util;


import net.adamslo.fontanaplayer.api.NetworkListener;
import net.adamslo.fontanaplayer.api.ServerStates;
import org.jetbrains.annotations.Nullable;

class FontanaServerProtocol {

   private ServerStates state = ServerStates.HELLO;

    @Nullable
    String processInput(@Nullable String theInput, @Nullable NetworkListener listener) {
       String theOutput = null;

       if (state == ServerStates.HELLO) {
           theOutput = ServerStates.HELLO.getString();
           state = ServerStates.LYRICS;
       } else if (state == ServerStates.LYRICS) {
           if (theInput != null && theInput.equalsIgnoreCase(ServerStates.BYE.getString())) {
               theOutput = ServerStates.BYE.getString();
               state = ServerStates.BYE;
           } else if (theInput != null) {
               theOutput = ServerStates.LYRICS.getString() + ": " + theInput;
               if (listener != null) {
                   listener.loadLyrics(theInput);
               }
           }
       }
       return theOutput;
   }
}
