package net.adamslo.fontanaplayer.util;


import be.lukin.poeditor.FileTypeEnum;
import be.lukin.poeditor.POEditorClient;
import net.adamslo.fontanaplayer.PlayerSetup;
import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.debug.FilePrinter;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Dictionary {

    private static final HashMap<DictionaryIdentifiers, String> data = new HashMap<DictionaryIdentifiers, String>();

    static {
        data.put(DictionaryIdentifiers.SERVERSTART, "Server zapnut");
        data.put(DictionaryIdentifiers.SERVERSTOP, "Server vypnut");
        data.put(DictionaryIdentifiers.CLIENTSTART, "Klient zapnut");
        data.put(DictionaryIdentifiers.CLIENTSTOP, "Klient vypnut");
        data.put(DictionaryIdentifiers.ERRORMP3, "Chyba pri nacitani hudby pro %s");
        data.put(DictionaryIdentifiers.NOTFOUNDMP3, "Hudba pro %s nenalezena");
        data.put(DictionaryIdentifiers.FOUNDMP3, "Hudba pro %s nactena");
        data.put(DictionaryIdentifiers.NOTFOUNDLYRICS, "Text pro %s nenalezen");
        data.put(DictionaryIdentifiers.FOUNDLYRICS, "Text pro %s nacten");
        data.put(DictionaryIdentifiers.TRANSLATIONMISSING, "Chyba pro preklad ->%s<-");
        data.put(DictionaryIdentifiers.BYESERVER, "Bye server said client");
        data.put(DictionaryIdentifiers.PAUSEABEL, "Mezerník");
        data.put(DictionaryIdentifiers.PAUSEDESCRIPTION, "Pozastavení přehrávání písně");
        data.put(DictionaryIdentifiers.IDENTIFIERSLABEL, "A až Z");
        data.put(DictionaryIdentifiers.IDENTIFIERSDESCRIPTION, "Zadávání identifikátorů písní");
        data.put(DictionaryIdentifiers.SCROLLLABEL, "šipka nahoru a dolů");
        data.put(DictionaryIdentifiers.SCROLLDESCRIPTION, "Posouvání textu");
        data.put(DictionaryIdentifiers.RESTARTLABEL, "Restart");
        data.put(DictionaryIdentifiers.OKLABEL, "");
        data.put(DictionaryIdentifiers.OKDESCRIPTION, "");
        data.put(DictionaryIdentifiers.CLOSELABEL, "");
        data.put(DictionaryIdentifiers.CLOSEDESCRIPTION, "");
        data.put(DictionaryIdentifiers.CLOSEBUTTON, "Zavřít (Esc)");
        data.put(DictionaryIdentifiers.MUSICON, "VYPNOUT HUDBU");
        data.put(DictionaryIdentifiers.MUSICOFF, "ZAPNOUT HUDBU");
        data.put(DictionaryIdentifiers.LYRICS, "TEXT");
        data.put(DictionaryIdentifiers.BYECLIENT, "Bye client said server");
        data.put(DictionaryIdentifiers.NOINPUTSERVER, "Neni vstupni tok dat pro server");
        data.put(DictionaryIdentifiers.UNABLEREADINPUTSTREAM, "Vstupni prod dat pro server neni platny");
        data.put(DictionaryIdentifiers.NOSERVER, "Klient se nemuze pripojit. Server nebezi.");
        data.put(DictionaryIdentifiers.ERRORFILEPRINTER, "Error in file printer: %s");
        data.put(DictionaryIdentifiers.ERRORFILETOKEN, "Error in translation server token file");
        data.put(DictionaryIdentifiers.POEAPIERROR, "POEditor API error");
        data.put(DictionaryIdentifiers.WINDOWTITLE, "FontánaPlayer");
        data.put(DictionaryIdentifiers.SERVERBUTTON, "\uD83D\uDC66 (1)");
        data.put(DictionaryIdentifiers.CLIENTBUTTON, "\uD83D\uDC68\u200D (2)");
    }

    @NotNull
    public static String get(@Nullable DictionaryIdentifiers identifier, @Nls Object... replaces) {
        if (replaces.length > 0) {
            if (data.get(identifier) == null) {
                return String.format(data.get(DictionaryIdentifiers.TRANSLATIONMISSING), identifier);
            } else {
                return String.format(data.get(identifier), replaces);
            }

        } else {
            return data.get(identifier) == null ? "null" : data.get(identifier);
        }

    }

    /**
     * downloads data over poeditor.com API, stores them locally as the primarry source for translations
     */
    public static void fetchTranslationsFromOnlineSource() {
        String apiKey = loadApiKey();
        POEditorClient client = null;
        if (apiKey != null) {
            client = new POEditorClient(apiKey);
        }
        String[] languages = PlayerSetup.getStringArray(SetupIdentifiers.POEDITORLANGUAGES);
        if (languages != null) {
            for (String language : languages) {
                String patternname = PlayerSetup.getString(SetupIdentifiers.POEDITORCACHEFILE);
                File file = new File(String.format(patternname == null ? "%s" : patternname, language));
                if (client != null) {
                    try {
                        client.export(PlayerSetup.getString(SetupIdentifiers.POEDITORPROJECT), language, FileTypeEnum.JSON, null, file);
                    } catch (Exception ex) {
                        ScreenPrinter.p(DictionaryIdentifiers.POEAPIERROR);
                    }
                }
                if (language.equals(PlayerSetup.getString(SetupIdentifiers.LANGUAGE))) {
                    fetchFile(file);
                }
            }
        }
    }

    private static void fetchFile(@Nullable File f) {

        if (f != null && f.exists()) {
            JSONParser parser = new JSONParser();
            try {
                JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(f.getAbsolutePath()));
                for (Object aJsonArray : jsonArray) {
                    JSONObject row = (JSONObject) aJsonArray;
                    String term = (String) row.get("term");
                    if (term != null) {
                        DictionaryIdentifiers id = DictionaryIdentifiers.getByName(term);
                        if (id != null) {
                            data.put(id, (String) row.get("definition"));
                        }
                    }

                }
            } catch (IOException e) {
                FilePrinter.p(e.getMessage());
            } catch (ParseException e) {
                FilePrinter.p(e.getMessage());
            }
        }
    }

    @Nullable
    private static String loadApiKey() {
        String content = null;
        try {
            String name = PlayerSetup.getString(SetupIdentifiers.POEDITORTOKENFILE);
            if (name != null) {
                File f = new File(name);
                if (f.exists()) {
                    content = FileUtils.readFileToString(f, "UTF-8");
                }
            }
        } catch (IOException e) {
            FilePrinter.p(DictionaryIdentifiers.ERRORFILETOKEN);
        }
        return content;
    }
}
