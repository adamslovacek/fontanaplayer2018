/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer.util;

import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.ServerStates;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FontanaClient {

    private Socket kkSocket;
    private PrintWriter out;

    public FontanaClient(@NotNull String host, @NotNull Integer port) {
        try {
            kkSocket = new Socket(host, port);
            out = new PrintWriter(kkSocket.getOutputStream(), true);

        } catch (ConnectException ex) {
            ScreenPrinter.p(DictionaryIdentifiers.NOSERVER);
        } catch (IOException ex) {
            Logger.getLogger(FontanaClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendMessageToLoad(@NotNull String idn) {
        if (idn.matches(ServerStates.BYE.getString())) {
            ScreenPrinter.p(DictionaryIdentifiers.BYESERVER);
        }
        if (out != null) {
            out.println(idn);
        }

    }

    public void disconnect() {
        try {
            if (out != null) {
                out.close();
            }
            if (kkSocket != null) {
                kkSocket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(FontanaClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
