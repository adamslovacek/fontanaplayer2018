/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */

package net.adamslo.fontanaplayer.util;


import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.NetworkListener;
import net.adamslo.fontanaplayer.api.ServerStates;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * (Description)
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 0.1,  &nbsp; 2.4.2010
 * @since SDK1.6
 */
public class FontanaServer implements Runnable {

    private final int port;
    private final NetworkListener l;
    private BufferedReader in;
    private PrintWriter out;
    private ServerSocket serverSocket;
    private Socket clientSocket = null;

    public FontanaServer(@NotNull Integer port, @NotNull NetworkListener l) {
        this.port = port;
        this.l = l;
    }

    private void run(@NotNull Integer port, @NotNull NetworkListener l) throws IOException {

        try {
            serverSocket = new ServerSocket(port);
            clientSocket = serverSocket.accept();
        } catch (IOException e) {
            return;
        }
        out = new PrintWriter(
                clientSocket.getOutputStream(), true);
        in = new BufferedReader(
                new InputStreamReader(
                        clientSocket.getInputStream()));
        String inputLine, outputLine;

        FontanaServerProtocol kkp = new FontanaServerProtocol();
        outputLine = kkp.processInput(null, l);
        out.println(outputLine);
        if (in != null) {
            try {
                while ((inputLine = in.readLine()) != null) {
                    outputLine = kkp.processInput(inputLine, l);
                    out.println(outputLine);
                    if (outputLine != null && outputLine.equals(ServerStates.BYE.getString())) {
                        l.loadLyrics(ServerStates.BYE.getString());
                        ScreenPrinter.p(DictionaryIdentifiers.BYECLIENT);
                        break;
                    }
            }
            } catch (SocketException e) {
                ScreenPrinter.p(DictionaryIdentifiers.UNABLEREADINPUTSTREAM);
            }
        } else {
            ScreenPrinter.p(DictionaryIdentifiers.NOINPUTSERVER);
        }

    }

    public void stop() {
        try {
            if (out != null) {
                out.close();
            }
            if (in != null) {
                in.close();
            }
            if (clientSocket != null) {
                clientSocket.close();
            }
            if (serverSocket != null) {
                serverSocket.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(FontanaServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void run() {
        try {
            run(port, l);

        } catch (IOException ex) {
            Logger.getLogger(FontanaServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
