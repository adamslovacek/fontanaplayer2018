/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.debug.FilePrinter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

class SingleSongLyrics {

    /**
     * Stored the only first page of loaded pdf file
     */
    private PDFPage firstPage;
    /**
     * Stores whole pdf file
     */
    private PDFFile pdffile;
    /**
     * current page
     */
    private int actualPageNumber;

    /**
     * Constructor opens the file and picks the first pdf page. When the
     * exception is generated the private variable page is set as  null. Else
     * represents rhe first page of opened pdf document
     *
     * @param file is the instance of File class specify the pdf file to be read
     */
    SingleSongLyrics(@NotNull File file) {
        firstPage = null;
        pdffile = null;
        try {
            FileChannel channel = new RandomAccessFile(file, "r").getChannel();
            pdffile = new PDFFile(channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size()));
            actualPageNumber = 0;
            firstPage = pdffile.getPage(0);
        } catch (IOException ex) {
            FilePrinter.p(ex);
        }
    }

    /**
     * returns page's height multipler. this value is used to render pdf files as
     * big as possible
     *
     * @return how many times is page heighter that its width
     */
    double getHeightMultipler() {
        if (PlayerSetup.getBoolean(SetupIdentifiers.SINGLEPAGEINTERFACE)) {
            return firstPage.getHeight() / firstPage.getWidth();
        } else {
            return pdffile.getPage(actualPageNumber).getHeight() / pdffile.getPage(actualPageNumber).getWidth();
        }
    }

    /**
     * use to get the first page
     *
     * @return the first pdf page
     */
    @Nullable
    PDFPage getFirstPage() {
        actualPageNumber = 0;
        return firstPage;
    }

    /**
     * use to get next page
     *
     * @return next page from pdf file
     */
    @Nullable
    PDFPage getNextPage() {
        if (PlayerSetup.getBoolean(SetupIdentifiers.SINGLEPAGEINTERFACE)) {
            return firstPage;
        }

        if (actualPageNumber + 1 < pdffile.getNumPages()) {
            actualPageNumber++;
        }

        return pdffile.getPage(actualPageNumber);
    }

    /**
     * use to get previous page
     *
     * @return previous page from pdf file
     */
    @Nullable
    PDFPage getPreviousPage() {
        if (PlayerSetup.getBoolean(SetupIdentifiers.SINGLEPAGEINTERFACE)) {
            return firstPage;
        }
        if (actualPageNumber > 0) {
            actualPageNumber--;
        }
        return pdffile.getPage(actualPageNumber);
    }

    /**
     * @return total count of pages
     */
    @NotNull
    Integer getNumberOfPages() {
        return pdffile == null?0:pdffile.getNumPages();
    }

    /**
     *
     * @return the actual page number
     */
    @NotNull
    Integer getCurrentPage() {
        return actualPageNumber;
    }

}
