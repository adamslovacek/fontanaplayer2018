/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer;

import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.debug.FilePrinter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Class includes a group of static setup variables for player
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 1.0,  &nbsp; 28-SEP-2009
 * @since SDK1.5
 */
public class PlayerSetup {

    private static final String setupFile = "setup.json";

    private static final HashMap<SetupIdentifiers, Object> data = new HashMap<SetupIdentifiers, Object>();

    private static final HashMap<String, Object> customData = new HashMap<String, Object>();

    static {
        /*
         * The text logo for application
         */
        data.put(SetupIdentifiers.LOGO, "Copyright (c) 2009-2017 Adam Slováček (adamslovacek@gmail.com).");
        /*
         * most used grey color
         */
        data.put(SetupIdentifiers.GREY, new String[]{"153", "153", "153"});
        /*
         * file that contains api key for poeeditor
         */
        data.put(SetupIdentifiers.POEDITORTOKENFILE, "poetoken.txt");
        /*
         * poeeditor project id
         */
        data.put(SetupIdentifiers.POEDITORPROJECT, "90355");
        /*
         * file that contains api key for poeeditor
         */
        data.put(SetupIdentifiers.POEDITORCACHEFILE, ".poetranslation.%s.json");
        /*
         * poeeditor languages
         */
        data.put(SetupIdentifiers.POEDITORLANGUAGES, new String[]{"cs", "en"});
        /*
         * poeeditor default language
         */
        data.put(SetupIdentifiers.LANGUAGE, "en");
        /*
         * Scrolling increment for lyrics pane in pixels
         */
        data.put(SetupIdentifiers.SCROLLINCREMENT, 150);
        /*
         * heightMultiplier says, how many times is pdf's height bigger than its
         * width. The purpose is to fit pdf to maximal possible dimensions
         * <p>
         * Height multipler is computed separatly for each and everyone pdf
         * file. If it is not possible to obtain it from pdf application uses
         * this default value
         */
        data.put(SetupIdentifiers.HEIGTHMULTIPLER, 2);
        /*
         * The identifier of the pdf file that is showeb when application loaded
         */
        data.put(SetupIdentifiers.INTROIDENTIFIER, "___");
        /*
         * path where songs are stored
         */
        data.put(SetupIdentifiers.SONGFOLDER, "mp3/");
        /*
         * path where lyrics are stored
         */
        data.put(SetupIdentifiers.LYRICSFOLDER, "pdf/");
        /*
         * Says, how many files is allowed to load.
         * for example if there maxFiles=3 only 3 pdf and mp3 files will be loaded
         * <p>
         * if maxFiles=-1 there is no limit and all the files are loaded
         */
        data.put(SetupIdentifiers.MAXFILES, 99999);
        /*
         * set up as true if target hardware is too weak. Do not se false if your
         * cpu frequency is bellow 1GHz and is PIII or worse.
         * <p>
         * if true no special features are used
         * <p>
         * It is not final just to force compiler change it trought all the classes
         */
        data.put(SetupIdentifiers.LIGHTWEIGHTMODE, false);
        /*
         * if true only first page from pdf file is read.
         */
        data.put(SetupIdentifiers.SINGLEPAGEINTERFACE, false);
    }

    static void loadSetupValues() {
        File f = new File(setupFile);
        if (f.exists()) {
            JSONParser parser = new JSONParser();
            try {
                JSONArray jsonArray = (JSONArray) parser.parse(new FileReader(f.getAbsolutePath()));
                parseJsonSetup(jsonArray);
            } catch (IOException | ParseException e) {
                FilePrinter.p(e.getMessage());
            }
        }
    }

    private static void parseJsonSetup(JSONArray dataJson) {
        for (Object aJsonArray : dataJson) {
            JSONObject row = (JSONObject) aJsonArray;
            String term = (String) row.get("identifier");
            if (term != null) {
                SetupIdentifiers id = SetupIdentifiers.getByName(term);
                if (id != null) {
                    data.put(id, row.get("value"));
                }
            }
        }
    }

    @Nullable
    public static String getString(@NotNull SetupIdentifiers identifier) {
        if (isCorrectClass(identifier, String.class)) {
            return String.valueOf(data.get(identifier));
        }
        return null;
    }

    @Nullable
    public static String[] getStringArray(@NotNull SetupIdentifiers identifier) {
        if (isCorrectClass(identifier, String[].class)) {
            return (String[]) data.get(identifier);
        }
        return null;
    }

    @NotNull
    static Integer getInteger(@NotNull SetupIdentifiers identifier) {
        if (isCorrectClass(identifier, Integer.class)) {
            return Integer.parseInt(data.get(identifier).toString());
        }
        return 0;
    }

    @NotNull
    static Boolean getBoolean(@NotNull SetupIdentifiers identifier) {
        if (isCorrectClass(identifier, Boolean.class)) {
            return Boolean.valueOf(data.get(identifier).toString());
        }
        return false;
    }

    @Nullable
    public static Color getColor(@NotNull SetupIdentifiers identifier) {
        if (isCorrectClass(identifier, Color.class)) {
            String[] specification = (String[]) data.get(identifier);
            return new Color(Integer.parseInt(specification[0]), Integer.parseInt(specification[1]), Integer.parseInt(specification[2]));
        }
        return null;
    }

    private static boolean isCorrectClass(@NotNull SetupIdentifiers identifier, @NotNull Class classid) {

        return identifier.getClassname().equals(classid.getName());
    }

    public static void setCustom(String key, Object value) {

        customData.put(key, value);
    }

    public static Object getCustom(String key) {
        return customData.get(key);
    }
}
