package net.adamslo.fontanaplayer;


import net.adamslo.fontanaplayer.api.*;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import net.adamslo.fontanaplayer.util.Dictionary;
import net.adamslo.fontanaplayer.util.FontanaClient;
import net.adamslo.fontanaplayer.util.FontanaServer;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;

public class NetworkFontanaPlayer extends FontanaPlayer {

    private final int port = 23232;
    private final String host = "localhost";
    private FontanaClient fontanaClient;
    private FontanaServer fontanaServer;
    private boolean Clientconnected = false;
    private boolean serveConnected = false;


    private NetworkFontanaPlayer() {
        NetrowkElements mode = (NetrowkElements) PlayerSetup.getCustom("mode");
        if (mode != null) {
            if (mode == NetrowkElements.SERVER) {
                toogleConnectionServer();
            } else if (mode == NetrowkElements.CLIENT) {
                toogleConnectionClient();
            }
        }
    }

    /**
     * Initializes player
     *
     * @param args the application command line arguments
     */
    public static void main(final String args[]) {
        PlayerSetup.loadSetupValues();
        Dictionary.fetchTranslationsFromOnlineSource();
        customInitByArgs(args);

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NetworkFontanaPlayer().setVisible(true);
            }
        });

    }

    private static void customInitByArgs(String[] args) {
        if (args.length > 0 && (args.length % 2) == 0) {
            for (int i = 0; i < args.length; i += 2) {
                String val = args[i + 1];
                switch (args[i]) {
                    case "-m":
                        setCustomMode(val);
                        break;
                    case "-w":
                        PlayerSetup.setCustom("windowmode", val.equals("window"));
                        break;
                    default:
                        break;
                }
            }

        }
    }

    private static void setCustomMode(String val) {
        if (val.toLowerCase().equals("server")) {
            PlayerSetup.setCustom("mode", NetrowkElements.SERVER);
        } else if (val.toLowerCase().equals("client")) {
            PlayerSetup.setCustom("mode", NetrowkElements.CLIENT);
        }
    }

    private void startServer() {
        if (!serveConnected) {
            ScreenPrinter.p(DictionaryIdentifiers.SERVERSTART);

            // Create the object with the run() method
            fontanaServer = new FontanaServer(port, new NetworkListener() {

                public void loadLyrics(@Nullable String identifier) {
                    if (identifier != null && identifier.matches(ServerStates.BYE.getString()) && serveConnected) {
                        toogleConnectionServer();

                    } else {
                        double heightMultipler = lyrics.getHeightMultipler();
                        if (heightMultipler == 0) {
                            heightMultipler = PlayerSetup.getInteger(SetupIdentifiers.HEIGTHMULTIPLER);
                        }
                        changeLyricsPanelDimensions(heightMultipler);
                        Rectangle rctg = pagePanel.getVisibleRect();
                        rctg.setLocation(0, 0);
                        pagePanel.repaint();
                        pagePanel.scrollRectToVisible(rctg);

                        pagePanel.showPage(lyrics.get(identifier));
                        numberOfPages = lyrics.getNumberOfPages();
                        currentPage = lyrics.getcurrentPage();
                        textLabel.setText(getTextLabel());

                        state = PlayerStates.PRINT_TEXT;
                        textLabel.setBackground(Color.green);
                        selectBorder(textLabel);
                    }

                }
            });

            Runnable runnable = fontanaServer;
            Thread thread = new Thread(runnable);
            thread.start();
        }
    }


    private void startClient() {

        if (!Clientconnected) {
            ScreenPrinter.p(DictionaryIdentifiers.CLIENTSTART);
            Runnable runnable = new Runnable() {
                public void run() {
                    fontanaClient = new FontanaClient(host, port);
                }
            };
            runnable.run();
        }
    }


    /**
     * Actions taken when application terminating. FontanaPlayer is stopped before it.
     */
    @Override
    protected void terminate() {
        if (fontanaServer != null) {
            fontanaServer.stop();
        }
        if (fontanaClient != null) {
            fontanaClient.sendMessageToLoad(ServerStates.BYE.getString());
            fontanaClient.disconnect();
        }

        super.terminate();
    }

    @Override
    protected void toogleConnectionClient() {
        if (Clientconnected) {
            clientStop();
            Clientconnected = false;
        } else {
            serverStop();
            colorButtons(NetrowkElements.SERVER, false);
            startClient();
            Clientconnected = true;
        }
        colorButtons(NetrowkElements.CLIENT, Clientconnected);
    }

    @Override
    protected void toogleConnectionServer() {

        if (serveConnected) {
            serverStop();
        } else {
            clientStop();
            colorButtons(NetrowkElements.CLIENT, false);
            startServer();
        }
        serveConnected = !serveConnected;
        colorButtons(NetrowkElements.SERVER, serveConnected);
    }

    private void colorButtons(NetrowkElements element, boolean on) {
        JButton button = getButton(element);
        if (button != null) {
            if (on) {
                button.setBackground(Color.green);
                button.setBorder(javax.swing.BorderFactory.createLineBorder(Color.black));
            } else {
                button.setBackground(blue);
                button.setBorder(javax.swing.BorderFactory.createLineBorder(PlayerSetup.getColor(SetupIdentifiers.GREY), 3));
            }
        }
    }

    private JButton getButton(NetrowkElements element) {
        JButton button = null;
        if (element == NetrowkElements.CLIENT) {
            button = connectButtonClient;
        } else if (element == NetrowkElements.SERVER) {
            button = connectButtonServer;
        }
        return button;

    }

    public void sendMessageToClient(String identifier) {
        if (Clientconnected && fontanaClient != null) {
            fontanaClient.sendMessageToLoad(identifier);

        }
    }

    private void serverStop() {
        if (serveConnected) {
            if (fontanaServer != null) {
                fontanaServer.stop();
                ScreenPrinter.p(DictionaryIdentifiers.SERVERSTOP);
            }
            fontanaServer = null;
        }
    }

    private void clientStop() {
        if (Clientconnected) {
            if (fontanaClient != null) {
                fontanaClient.disconnect();
                ScreenPrinter.p(DictionaryIdentifiers.CLIENTSTOP);
            }
            fontanaClient = null;
        }
    }
}
