/*
 * Copyright (c) 2009-2017 Adam Slovacek. All rights reserved.
 */
package net.adamslo.fontanaplayer;

import javazoom.jlgui.basicplayer.BasicPlayer;
import javazoom.jlgui.basicplayer.BasicPlayerException;
import javazoom.jlgui.basicplayer.BasicPlayerListener;
import net.adamslo.fontanaplayer.api.DictionaryIdentifiers;
import net.adamslo.fontanaplayer.api.SetupIdentifiers;
import net.adamslo.fontanaplayer.debug.FilePrinter;
import net.adamslo.fontanaplayer.debug.ScreenPrinter;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.HashMap;

/**
 * Instance of this class is a simple MP3 player. The playlist is built from
 * the files placed in directory specified in constructor.
 *
 * Class basicly extend the BasicPlayer by adding a simple playlist.
 *
 * @author Adam Slovacek
 * @author <a href="mailto:adamslovacek@gmail.com">adamslovacek@gmail.com</a>
 * @version 1.0,  &nbsp; 28-SEP-2009
 * @since SDK1.5
 */
class MP3Player {

    /**
     * basicPlayer is an instance of
     * <a href="http://www.javazoom.net/jlgui/jlgui.html">jlgui player</a>
     * jlgui player  is under  LGPL licence
     *
     * it's methots are called for a basic functionality such as play, stop
     * or resume
     *
     */
    private final BasicPlayer basicPlayer;
    /**
     * Map to store the playlist files
     */
    private final HashMap<String, File> songFileList;
    /**
     * Says if the player is paused or not
     */
    private boolean paused = false;
    /**
     * if the song is played the current song is it's representation
     */
    private File currentSong;

    /**
     * Constructor used to create this object.
     * Files are placed in the folder specified by parameter. No other folders
     * include subforders are browsed to find the files.
     * The MP3 file is recognized by the name lenght and is's extension. Be
     * careful about placing *.mp3 files if are not actualy MP3 files
     *
     */
    MP3Player() {
        songFileList = new HashMap<String, File>();
        basicPlayer = new BasicPlayer();

        addFiles();
    }

    private void addFiles() {


        String folder = PlayerSetup.getString(SetupIdentifiers.SONGFOLDER);

        if (folder != null) {
            File myDir = new File(folder);
            if (myDir.exists() && myDir.isDirectory()) {
                loadFileToList(myDir);
            }
        }
    }

    private void loadFileToList(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            int max = Math.min(files.length, PlayerSetup.getInteger(SetupIdentifiers.MAXFILES));
            for (int i = 0; i < max; i++) {
                addToFileList(files[i]);
            }
        }
    }


    private void addToFileList(File file) {
        String extension = FilenameUtils.getExtension(file.getName());
        if (file.getName().length() > 4 && extension.compareTo("mp3") == 0 && !file.isDirectory()) {
            songFileList.put(file.getName().substring(0, 3).toLowerCase(), file);
        }
    }

    /**
     * Stops the current song. The currentSong variable is set to null, because
     * it is not possible to continue playing
     */
    void stop() {
        try {
            basicPlayer.stop();
            currentSong = null;
        } catch (BasicPlayerException ex) {
            FilePrinter.p(ex);
        }
    }

    /**
     * Pauses the current song and sets the paused variable.
     */
    void pause() {
        try {
            basicPlayer.pause();
            paused = true;
        } catch (BasicPlayerException ex) {
            FilePrinter.p(ex);
        }
    }

    /**
     * Resume the current song and sets the paused variable.
     */
    void resume() {
        paused = false;
        try {
            basicPlayer.resume();
        } catch (BasicPlayerException ex) {
            FilePrinter.p(ex);
        }
    }

    /**
     * Says if the song/player is pauset or not
     *
     * @return true if paused, else false
     */
    boolean isPaused() {
        return paused;
    }

    /**
     *This method picks from the songFileList the song specified by parameter.
     * If there is not such a song it stops itselfs. Otherwise opens the new song
     * (file) and palys it.
     *
     * @param identifier  song file identifier
     * @return true if song exists, else false
     */
    boolean play(@Nullable String identifier) {
        currentSong = songFileList.get(identifier);

        try {
            if (currentSong == null) {
                basicPlayer.stop();
                ScreenPrinter.p(DictionaryIdentifiers.NOTFOUNDMP3, identifier);
                return false;
            }

            paused = false;
            basicPlayer.open(currentSong);
            basicPlayer.play();

        } catch (BasicPlayerException ex) {
            ScreenPrinter.p(DictionaryIdentifiers.ERRORMP3, identifier);
            FilePrinter.p(ex);
        }

        ScreenPrinter.p(DictionaryIdentifiers.FOUNDMP3, identifier);
        return true;
    }


    /**
     * addds player listener. use if lightWeight mode if false
     * @param basicPlayerListener listener for player
     */
    void addBasicPlayerListener(@NotNull BasicPlayerListener basicPlayerListener) {
        basicPlayer.addBasicPlayerListener(basicPlayerListener);

    }

    /**
     *removes listener from basic player
     *
     * @param basicPlayerListener listener to be removed
     */
    void removeBasicPlayerListener(@NotNull BasicPlayerListener basicPlayerListener) {
        basicPlayer.removeBasicPlayerListener(basicPlayerListener);
    }

    /**
     * restarts curently played song
     */
    void restart() {
        try {
            basicPlayer.seek(0);
        } catch (BasicPlayerException ex) {
            FilePrinter.p(ex);
        }
    }
}
